let express = require('express')
const fs = require('fs')
const path = require('path')
let router = express.Router()
const db = require('../lib/sqlite3')
const jwt = require('../lib/jwt.js')
let vInstalled = require('debug')('lifecycle:installed')
let vUninstalled = require('debug')('lifecycle:uninstalled')

/*
 * This route will redirect anything that goes to the root of the site to the descriptor.
 */
router.get('/', function(req, res) {
  res.redirect('/descriptor')
})

/*
 * A route for the descriptor.
 *
 * We'll read the descriptor file into a variable and parse it as JSON data.
 * We'll then update the baseUrl with the url of the host that's running it.
 * This makes it easy as we'll be unsure what the baseUrl will be when we create the app in app management.
 * Then we'll set the contentType to application/json and send the completed JSON out to be served.
 */
router.get('/descriptor', (req, res) => {
  let descriptor = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../app-descriptor.json')).toString()
  )
  descriptor.baseUrl = 'https://' + req.headers.host
  res.contentType = 'application/json'
  res.send(descriptor)
  res.end()
})

/*
 * A route for the installed webhook.
 *
 * In this route we're listening in for POST requests to /installed.
 * This is because Stride will post a webhook to the endpoint we specified in the descriptor for installed.
 * Stride will send a payload of data for us to consume and save off.
 * This data will provide us with everything we need to know about the conversation we've been in stalled in.
 */
router.post('/installed', jwt.verifyRequest, (req, res) => {
  // Let's print out some info to the console to show us where we're at and to see the data being passed in.
  vInstalled('Webhook was fired by Stride, our app has picked it up.')
  vInstalled(req.body)

  // context.cloudId        - the site id that contains the conversation that the app was installed in.
  // context.userId         - the user id that installed the app.
  // context.conversationId - the conversation id that the app was installed in.
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }
  db.serialize(() => {
    db.run(
      `INSERT INTO
      installed
     VALUES
     (
       "${context.cloudId}",
       "${context.userId}",
       "${context.conversationId}")`,
      err => {
        if (err) {
          vInstalled('There is an error')
          vInstalled(err)
        }
      }
    )

    // Let's print this saved data out to the terminal
    vInstalled('Installation payload data saved.')
    db.get(
      `SELECT
        *
       FROM
        installed
       WHERE
        cloudId = "${context.cloudId}"`,
      (err, row) => {
        if (err) {
          vInstalled(err)
        }
        vInstalled(row)
      }
    )
  })

  res.end()
})

/*
 * A route for the uninstalled webhook.
 *
 * In this route we're listening in for POST requests to /uninstalled.
 * This is because Stride will post a webhook to the endpoint we specified in the descriptor for uninstalled.
 * Stride will send a payload of data for us to consume and use to remove data associated with this conversation.
 */
router.post('/uninstalled', jwt.verifyRequest, (req, res) => {
  // Let's print out some info to the console to show us where we're at and to see the data being passed in.
  vUninstalled('Webhook was fired by Stride, our app has picked it up.')
  vUninstalled(req.body)

  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  db.serialize(() => {
    // Let's first see the information in the datastore that we have for the payload that we received above.

    db.get(
      `SELECT
        *
       FROM
        installed
       WHERE
        cloudId = "${context.cloudId}"`,
      (err, row) => {
        vUninstalled("Let's pull the information from the data store.")
        if (err) {
          vUninstalled(err)
        }
        vUninstalled(row)
      }
    )

    // now let's delete it
    db.run(
      `DELETE FROM
      installed
     WHERE
      cloudId = "${context.cloudId}"
     AND
      conversationId = "${context.conversationId}"`,
      err => {
        if (err) {
          vUninstalled('We caught an error when deleting.')
          vUninstalled(err)
        }
      }
    )

    // Let try pulling the info again to verify that it was indeed deleted.  You wouldn't do this in production.

    db.get(
      `SELECT
        *
       FROM
        installed
       WHERE
        cloudId = "${context.cloudId}"`,
      (err, row) => {
        if (err) {
          vUninstalled(err)
        }
        vUninstalled(
          "Repull the data from the datastore, we shouldn't get anything back"
        )
        if (row != undefined) {
          vUninstalled(row)
        } else {
          vUninstalled('Nothing found!')
        }
      }
    )
  })
  res.end()
})

module.exports = router
